module cableDuctSupport(height)
{
    union()
    {
        cube([ 160, 2, height ]);
        translate([ 0, -10, 0 ]) cube([ 2, 10, height ]);
        for (i = [ 0, 69 ])
        {
            translate([ i, 2, 0 ]) cube([ 2, 10, height ]);
        }
    }
}
